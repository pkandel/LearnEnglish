<?php 
	class User extends Base_Db{

		protected static $the_table = "user";
		protected static $the_table_fields = array('username','email','password','isadmin');
		public $id;
		public $username;
        public $email;
        public $password;
		public $isadmin;

	}
 ?>