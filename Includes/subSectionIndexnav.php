<?php session_start(); ?>
    <style>
    /* - changin media query for default navbar collapse --- */
    @media (max-width: 990px) {
/* For toggle button  */
 #toggle{
                width:45px;
                height: 32px;
                border: 2px solid #2a2a2a;
                border-radius: 6px;
                background-color: #2a2a2a;
            }
       #first{
               /* border-bottom: 3px solid #2a2a2a; */
                border-bottom: 2px solid white;
                border-radius: 8px;
                margin-top: 9px;
                margin-left: 6px;
                margin-right: 10px;
                
            }
                #second{
                border-bottom: 2px solid white;
                border-radius: 8px;
                margin-top: 2px;
                 margin-left: 6px;
                margin-right: 10px;
            }
                #third{
                border-bottom: 2px solid white;
                border-radius: 8px;
                margin-top: 2px;
                 margin-left: 6px;
                margin-right: 10px;
            }


/* For toggle button  */
        /* changes default navbar brand position */
    .navbar-brand{
      left:100px;
        margin-right:100px;
        position: relative;
    }
    }

    /* -changin media query for default navbar collapse -- */
    /* -changin navbar multiple dropdown -- */
.dropdown-submenu {
        position: relative;
    }

        .dropdown-submenu > .dropdown-menu {
            top: 0;
            left: 100%;
            margin-top: -6px;
            margin-left: -1px;
            -webkit-border-radius: 0 6px 6px 6px;
            -moz-border-radius: 0 6px 6px;
            border-radius: 0 6px 6px 6px;
        }

        .dropdown-submenu:hover > .dropdown-menu {
            display: block;
        }

        .dropdown-submenu > a:after {
            display: block;
            content: " ";
            float: right;
            width: 0;
            height: 0;
            border-color: transparent;
            border-style: solid;
            border-width: 5px 0 5px 5px;
            border-left-color: #ccc;
            margin-top: 5px;
            margin-right: -10px;
        }

        .dropdown-submenu:hover > a:after {
            border-left-color: #fff;
        }

        .dropdown-submenu.pull-left {
            float: none;
        }

            .dropdown-submenu.pull-left > .dropdown-menu {
                left: -100%;
                margin-left: 10px;
                -webkit-border-radius: 6px 0 6px 6px;
                -moz-border-radius: 6px 0 6px 6px;
                border-radius: 6px 0 6px 6px;
            }

    @media (min-width: 768px) {
     .drop:hover > .dropdown-menu {
            display: block;   
         
    }
    #pte:hover > .dropdown-menu{
        display: block;
    }
}
        

    </style>
    <?php 
    require_once("init.php");
    $mainSection = MainSection::Get_All();
    $secondSection  = SecondSection::Get_All();
    ?>
     <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button style="border:none !important;color:white;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" >
                    <span class="sr-only">Toggle navigation</span>
                    <text style="color:white;font-size:10px">MENU</text>
                    <span style="color:white;font-size:10px" class="glyphicon glyphicon-chevron-down"></span>
                </button>
                <a href="#">
                <div id="toggle" style="padding-left:6px;float:left;position:fixed;margin-left:5px;margin-top:10px;">
                  <div id="first"></div>
                  <div id="second"></div>
                  <div id="third"></div>
                </div>
                   </a>
               <a class="navbar-brand" rel="home" href="/index.php" title="Homepage">LearnEnglish</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li id="pte">
                        <a data-toggle="dropdown" href="#">PTE <b class="caret"></b></a>
                        <ul class="dropdown-menu multi-level">
                            <!-- Web-->

                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dictionary</a>
                                <ul class="dropdown-menu">
                                    <li><a href="/Word/AllWords.php">Word</a></li>
                                    <li><a href="/Word/AllPhrasalVerb.php">Phrasal Verb</a></li>            
                                    <li><a href="/Word/collocation.php">Collocation</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Practice</a>
                                <ul class="dropdown-menu">
                                    <li><a href="/Essay/Essays.php">Essays</a></li>
                                    <li><a href="/Speaking/graphList.php">Speaking Graph</a></li>            
                                    <li><a href="/Essay/summary.php">Summarises</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                             <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Template</a>
                                <ul class="dropdown-menu">
                                    <li><a href="/Template/templateGraphList.php">Speaking Graph</a></li>
                                    <li><a href="/Template/templateEssay.php">Essay</a></li>            
                                    <li><a href="#">Summarises</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                        <li >
                        <a href="/PteGeneral/list.php" >
                        <ul style="padding-left: 0" class="dropdown-submenu">
                        Other
                        </ul></a></li>
                        </ul>
                    </li>
                   <?	if( isset($_SESSION['admin']) && $_SESSION['admin'] === 1) {  ?>
                    <li class="drop">
                        <a id="note" href="/Note.php">Notes <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                             <?php foreach($mainSection as $ms){ ?>
                            <li class="dropdown-submenu">
                               
                                <a href="/Note/secondsectionindex.php?id=<?php echo $ms->id; ?>" ><?php echo $ms->name; ?></a>
                                <ul class="dropdown-menu">
                                    <?php foreach($secondSection as $ss){ 
                                        if($ss->mainsectionId == $ms->id){
                                    ?>
                                   <li><a href="/Note/subSectionIndex.php?id=<?php echo $ss->id; ?>" ><?php echo $ss->name; ?></a></li> 
                                    <?php } }?>
                                </ul>
                                <li class="divider"></li> 
                            </li>
                             <?php }} ?>
                              
                        </ul>
                    </li>

                </ul>
        <ul class="nav navbar-nav navbar-right">
                 <?php if( isset($_SESSION['admin']) && $_SESSION['admin'] === 1) {  ?>
         <li><a href="/Security/logout.php">Log out </a></li>
         <?php  } else { ?>
         <li><a href="/Security">Log In </a></li>
         <?php  }?>
<!--
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
-->
    </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div> 
<script type="text/javascript">
$(function(){
    
    if($(window).width() < 768)
    {
        $(".drop > a").attr("data-toggle","dropdown");
    }

});
</script>
