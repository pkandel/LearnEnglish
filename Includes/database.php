<?php 
require_once("config.php");
class Database{
	private $connection;

	function __construct()
	{
		$this->open_db_connection();
	}

	function get_connection()
	{
		return $this->connection;
	}

	public function open_db_connection()
	{
		$this->connection = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME) or die("connection failed");
	}

	public function query($sql)
	{
		$result = mysqli_query($this->connection, $sql);
		return $result;
	}

	private function confirm_query($result)
	{
		if($result)
		{
			die("query failed");
		}
	}

	public function escape_string($string)
	{
		$escaped_string = mysqli_real_escape_string($this->connection,$string);
		return $escaped_string;
	}

	public function insert_id()
	{
		return mysqli_insert_id($this->connection);
	}
}
$database = new Database();
 ?>