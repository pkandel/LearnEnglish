<?php 
//if we forget to include the class.php it will auto load the file
	function __autoload($class)
	{
		$class = strtolower($class);
		$the_path = "Includes/{$class}.php";
		if(file_exists($the_path))
		{
			require_once($the_path);
		}
		else
		{
			die("The file name {$class}.php was not found..");
		}
	}


 ?>