<?php
class SubSection extends Base_Db{

    protected static $the_table = "subsection";
    protected static $the_table_fields = array('name','content','secondsectionId');
    public $id;
    public $name;
    public $content;
    public $secondsectionId;
}
?>