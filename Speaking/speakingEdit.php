<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php require("../Includes/head.php"); ?>
<!--rich text editor -->
<script src="//cdn.ckeditor.com/4.6.0/full/ckeditor.js"></script>
</head>

<body>
    <!-- Navigation  -->
    <?php 
    include "../Includes/nav.php";
if(isset($_SESSION['admin']) && $_SESSION['admin'] === 1)
                {  

     $query = "SELECT * FROM speakinggraph WHERE id='".$_GET['key']."'";
    $results = mysqli_query($conn, $query);
    
        $pie = null;
        $line = null;
        $picture = null;
        $lc = null;
        $map = null;
        $oChart = null;
        $tChart = null;
        $tTable = null;
        $bar = null;
    while ($result = mysqli_fetch_array($results))
    {
        if($result['category'] == "Pie")
        {
             $pie = "selected";
        }
        elseif($result['category'] == "Graph")
        {
            $line = "selected";
        }
          elseif($result['category'] == "Picture")
        {
            $picture = "selected";
        }
          elseif($result['category'] == "Life Cycle")
        {
            $lc = "selected";
        }
        elseif($result['category'] == "Map")
        {
            $map = "selected";
        }
          elseif($result['category'] == "Org Chart")
        {
            $oChart = "selected";
        }
          elseif($result['category'] == "Table Chart")
        {
            $tChart = "selected";
        }
          elseif($result['category'] == "Time Table")
        {
            $tTable = "selected";
        }
          else
        {
            $bar = "selected";
        }
        echo '        <div class="body_wrapper container">
            <form action="#" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <label for="exampleTextarea">Question</label>
                            <textarea class="form-control" id="question" name="question" rows="3">'.$result['question'].'</textarea>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="exampleTextarea">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="15">'.$result['description'].'</textarea>
                            <script>
                                CKEDITOR.replace( "description" );
                            </script>
                        </div>
                          <div class="form-group">
                    <label for="essayType">Image Type</label>
                    <select class="form-control" id="type" name="type">
                        <option value = "Pie" '.$pie.'>Pie Chart</option>
                        <option value = "Graph" '.$line.'>Graph</option>
                        <option value = "Picture" '.$picture.'>Picture</option>
                        <option value = "Life Cycle" '.$lc.'>Life Cycle</option>
                        <option value = "Map" '.$map.'>Map</option>
                        <option value = "Org Chart" '.$oChart.'>Org Chart</option>
                        <option value = "Table Chart" '.$tChart.'>Table Chart</option>
                        <option value = "Time Table" '.$tTable.'>Time Table</option>

                    </select>
                </div>
                        <button type="submit" name="submit" id="submit" class="btn btn-primary" style="float:right">Save</button>
                    </div>

                </div>

            </form>
        </div>';
    }} else {
   echo "<div style='margin-top:80px; color: red'> <h1> You are not authorised </h1></div>";
  }
   ?>

<?php require_once("../Includes/footer.php"); ?>

</body>

</html>

<?php
 require_once("../Includes/config.php");
if (isset($_POST['submit']))
{
    $question = $_POST['question'];
    $description = $_POST['description'];
    $category = $_POST['type'];
    $date = date("Y-m-d H:i:s");
    
    $query = "UPDATE speakinggraph SET question='$question', description='$description', category='$category', date='$date' WHERE id='".$_GET['key']."'" or die("query failed");
    $results = mysqli_query($conn, $query);
    if ( false===$results ) 
       {
            printf("error: %s\n", mysqli_error($conn));
        }
        else 
        {
            //show that word is added on the database
            echo '<script type="text/javascript">window.location = "/Speaking/speakingDetails.php?id='.$_GET['key'].'"</script>';
            
            die();
        }
}

?>
