<!DOCTYPE html>
<html lang="en">

<head>
<?php require("../Includes/head.php"); ?>
        <!-- css for dropdown -->
    <style>
    #dropdown
        {
    float:right;
    display:block;
    clear:left;
    }
    
    </style>
</head>

<body>
        <!-- Navigation  -->
   <?php 
    include "../Includes/nav.php";

    //for dropdown
    
    $selected ='';
    $query = '';
    $results = '';
    function get_options($selected){
    $categories = array('All Graph' => 'All Graph','Pie Chart' => 'pie', 'Line Graph' => 'line', 'Picture' => 'picture', 'Life Cycle' => 'lc', 'Map' => 'map');
    $options ='';
    while(list($k, $v) = each($categories))
    {
        if($selected === $v){
            $options .= '<option value="'.$v.'" selected>'.$k.'</option>';
        }
        else{
            $options .= '<option value="'.$v.'">'.$k.'</option>';
        }
        
        
    }
    return $options;

    }
    if(isset($_POST["dropdown"])){

     $selected = $_POST["dropdown"];
        if($selected == "pie")    
        {
            //do discussive stuff
            $query = "SELECT * FROM speakinggraph where category='pie' and istemplate='no' ORDER BY date DESC" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died 1");
        }
        else if($selected == "line")
        {
            //do Argument stuff
            $query = "SELECT * FROM speakinggraph WHERE category='line' and istemplate='no'  ORDER BY date DESC" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died 2");
        }
        else if($selected == "picture")
        {
            //do Problem and Solution stuff
            $query = "SELECT * FROM speakinggraph WHERE category='picture' and istemplate='no'  ORDER BY date DESC" or die("query died" );
            $results = mysqli_query($conn, $query) or die("results died 3");
        }
                else if($selected == "lc")
        {
            //do Problem and Solution stuff
            $query = "SELECT * FROM speakinggraph WHERE category='lc' and istemplate='no'  ORDER BY date DESC" or die("query died" );
            $results = mysqli_query($conn, $query) or die("results died 3");
        }
                else if($selected == "map")
        {
            //do Problem and Solution stuff
            $query = "SELECT * FROM speakinggraph WHERE category='map' and istemplate='no'  ORDER BY date DESC" or die("query died" );
            $results = mysqli_query($conn, $query) or die("results died 3");
        }
        else
        {
           //all words 
            $query = "SELECT * FROM speakinggraph WHERE istemplate='no'  ORDER BY date DESC" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died 4");
        }
    }
    else{
         $query = "SELECT * FROM speakinggraph WHERE istemplate='no'  ORDER BY date DESC" or die("query died ");
        $results = mysqli_query($conn, $query) or die("results died 5");
    }
    //echo get_options();
    
    
    echo '<div class="body_wrapper container">
   <a href="/Speaking/speakingGraph.php"><button class="btn btn-primary" >Add Graph</button></a>
    <form action="'.$_SERVER["PHP_SELF"].'" method="POST" >
    <select id="dropdown" name="dropdown" onchange="this.form.submit();">
       '.get_options($selected).'
    </select>
    </form>

</div><br />';
    
    while ( $result = mysqli_fetch_array($results))
    {
        echo '  
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-body">
                <a href="speakingDetails.php?id='.$result['id'].'">
                    Question '.$result['id'].' :-
                    '.$result['question'].'
                    </a>
                </div>
            </div>
        </div>';
    }
   ?>
 
<?php require_once("../Includes/footer.php"); ?>
</body>

</html>