<!DOCTYPE html>
<html lang="en">

<head>
<?php require("../Includes/head.php"); ?>

<!--rich text editor -->
<script src="//cdn.ckeditor.com/4.6.0/full/ckeditor.js"></script>
</head>

<body>
    <!-- Navigation  -->
    <?php 
    include "../Includes/nav.php";
   ?>
        <div class="body_wrapper container">
            <form action="" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <label for="exampleTextarea">Image Title</label>
                            <textarea class="form-control" id="question" name="question" rows="2"></textarea>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="exampleTextarea">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="15"></textarea>
                            <script>
                                CKEDITOR.replace( "description" );
                            </script>
                        </div>
                          <div class="form-group">
                    <label for="essayType">Image Type</label>
                    <select class="form-control" id="type" name="type">
                        <option value="pie">Pie Chart</option>
                        <option value="line">Line Graph</option>
                        <option value="picture">Picture</option>
                        <option value="map">Map</option>
                        <option value="lc">LifeCycle</option>

                    </select>

                </div>
                        <button type="submit" name="submit" id="submit" class="btn btn-primary" style="float:right">Save</button>
                    </div>

                </div>

            </form>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>

</html>

<?php
 require_once("../Includes/config.php");
if (isset($_POST['submit']))
{
    $question = $_POST['question'];
    $description = $_POST['description'];
    $category = $_POST['type'];
    $date = date("Y-m-d H:i:s");
    $istemplate = "no";
    
    $query = "INSERT INTO speakinggraph (question,description,category,date,istemplate) values ('$question','$description','$category','$date','$istemplate')" or die("query failed");
    $results = mysqli_query($conn, $query);
    if ( false===$results ) 
       {
            printf("error: %s\n", mysqli_error($conn));
        }
        else 
        {
            //show that word is added on the database
               echo '<script type="text/javascript">window.location = "/Speaking/graphList.php"</script>';
            
            die();
            
        }
}

?>
