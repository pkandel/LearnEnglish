<!DOCTYPE html>
<html lang="en">

<head>
<?php require_once("../Includes/head.php"); ?>
    <!-- for text editor -->
    <script src="//cdn.ckeditor.com/4.5.11/basic/ckeditor.js"></script>

</head>

<body>
    <?php include "../Includes/nav.php" ?>
        <div class=" body_wrapper container">

            <form method="post" action="" onsubmit="return checkValidation()">
                <!-- later on i will use this to categorise for different user -->

                <!--
                <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            -->
                <div class="form-group">
                    <label for="word"> New Word</label>
                    <input type="text" class="form-control" id="word" name="word" placeholder="word">
                </div>
                <div class="form-group">
                    <label for="word"> Description </label>
                    <input type="text" class="form-control" id="meaning" name="meaning" placeholder="description" rows="1">
                   <script>
                      //   CKEDITOR.replace( 'meaning' );
                     </script>
                </div>
                <div class="form-group">
                    <label for="wordType">Word Type</label>
                    <select class="form-control" id="type" name="type">
                       <option value="" >Select Word Type</option>
                        <option value="noun" >Noun</option>
                        <option value="verb">Verb</option>
                        <option value="adjective">Adjective</option>
                        <option value="adverb">Adverb</option>

                    </select>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Different Forms</div>
                    <div class="form-group col-sm-3">
                        <label for="word"> Adverb </label>
                        <input type="text" class="form-control" id="adverb" name="adverb" placeholder="Adverb">
                    </div>
                    <div class="form-group col-sm-3">
                        <label for="word"> Noun </label>
                        <input type="text" class="form-control" id="noun" name="noun" placeholder="Noun">
                    </div>
                    <div class="form-group col-sm-3">
                        <label for="word"> Verb </label>
                        <input type="text" class="form-control" id="verb" name="verb" placeholder="Verb">
                    </div>
                    <div class="form-group col-sm-3">
                        <label for="word"> Adjective </label>
                        <input type="text" class="form-control" id="adjective" name="adjective" placeholder="Adjective">
                    </div>
                </div>
                <div class="form-group">
                    <label for="synonym">Synonym</label>
                    <input type="text" class="form-control" id="synonym" name="synonym" placeholder="synonym">

                </div>
                <div class="form-group">
                    <label for="exampleTextarea">Example Sentence</label>
                    <textarea class="form-control" id="sentence" name="sentence" rows="1"></textarea>
                     <script>
                        CKEDITOR.replace( 'sentence' );
                    </script>

                </div>
                <!-- this will be used to insert file later on if needed -->
                <!--
                <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>
                </div>
                -->
                <fieldset class="form-group">
                    <legend>Category</legend>
                    <label class="radio-inline">
                        <input type="radio" name="category" value="hard" checked>Hard</label>
                    <label class="radio-inline">
                        <input type="radio" name="category" value="comfortable">Comfortable</label>
                    <label class="radio-inline">
                        <input type="radio" name="category" value="easy">Easy</label>
                </fieldset>

                <fieldset class="form-group">
                    <legend>Focus on</legend>
                    <label class="checkbox-inline">

                        <input type="checkbox" name="focusOn[]" value="meaning">Meaning</label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="focusOn[]" value="pronunciation">Pronunciation</label>
                </fieldset>
                <button type="submit" name="submit" id="submit" class="btn btn-primary">Add New Word</button>
            </form>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        </div>
</body>
<div class="footer">
    <p></p>
</div>

</html>


<!-- later to be make separate php file -->
<?php
    //if the form is submitted
    if (isset($_POST['submit']))
    {
         $name = $_POST['word'];
         $type = $_POST['type'];
         $synonym = $_POST['synonym'];
         $sentence = $_POST['sentence'];
         $category = $_POST['category'];
        $meaning = $_POST['meaning'];
        $focus = $_POST['focusOn'];
        $adverb = $_POST['adverb'];
        $adjective = $_POST['adjective'];
        $verb = $_POST['verb'];
        $noun = $_POST['noun'];
        $date = date("Y-m-d H:i:s");
        if($focus[0] == NULL || $focus[1] == NULL) //// March 10, 2001, 5:16 pm
        {
            $focusOn = $focus[0].$focus[1];
        }
        else
        {
            $focusOn = $focus[0].' , '. $focus[1];
        }
     
       
        //Do some validation and escape sequence things
        
        $query = "INSERT INTO word 
                                  (name,type,synonym,sentence,category,description,adverb,noun,verb,adjective,focuson,dateAdded)
                                  values 
                                  ('$name','$type', '$synonym','$sentence','$category','$meaning','$adverb','$noun','$verb','$adjective','$focusOn','$date')"
            or die("query failed");
        

         
        $results = mysqli_query($conn, $query);
       
        
       if ( false===$results ) 
       {
            printf("error: %s\n", mysqli_error($conn));
        }
        else 
        {
            //show that word is added on the database
             echo '<script type="text/javascript">window.location = "AllWords.php"</script>';
            
            die();
            
        }
//        
    }
    
?>