<!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home</title>

        <!--  bootstrap Stylesheet and theme  -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <script src="//cdn.ckeditor.com/4.5.11/basic/ckeditor.js"></script>
    
    </head>

    <body>

        <?php 
        include "../Includes/nav.php";
         require_once("../Includes/config.php");
        $displayQuery = 'SELECT * FROM phrasalverb WHERE id = "'.$_GET['key'].'"' or die("query died");
    
        $displayResults = mysqli_query($conn, $displayQuery);
        if ($displayResults===false)
        {
          printf("error: %s\n", mysqli_error($conn));
        }
        while ($dResult = mysqli_fetch_array($displayResults)){
             $displayname = $dResult['name'];
            $displayCategory = $dResult['category'];
          
            $displayDescription = $dResult['description'];
         
            $displaySentence = $dResult['sentence'];
                
            
            
        
        //to make the right category checked
        if($displayCategory == "hard")
                    {
                        $hard = "checked";
                      
                    }
                    elseif($displayCategory == "comfortable")
                    {
                        $comf = "checked";
                     
                    }
                    else
                    {
                        $easy = "checked";
                     
                   }
                    echo '<div class="container">
           <form method="post" action="" onsubmit="return checkValidation()">
                    <div class="form-group">
                        <label for="word"> New Word</label>
                        <input type="text" class="form-control" id="word" name="word" value="'.$displayname.'" />
                    </div>
                    <div class="form-group">
                        <label for="word"> Description </label>
                        <input type="text" class="form-control" id="meaning" name="meaning" value="'.$displayDescription.'">
                    </div>

                    <div class="form-group">
                        <label for="exampleTextarea">Example Sentence</label>
                        <textarea class="form-control" id="sentence" name="sentence" rows="3"
                        >'.$displaySentence.'</textarea>
                        <script>
                            CKEDITOR.replace( "sentence" );
                        </script>
                    </div>
                    <fieldset class="form-group">
                   <legend>Category</legend>
                        <label class="radio-inline">
                            <input type="radio" name="category" value="hard" '.$hard.'>Hard</label>
                        <label class="radio-inline">
                            <input type="radio" name="category" value="comfortable"'.$comf .'>Comfortable</label>
                        <label class="radio-inline">
                            <input type="radio" name="category" value="easy"'.$easy .'>Easy</label>
                    </fieldset>
                    <button type="submit" name="submit" id="submit" class="btn btn-primary">Update</button>
                </form>
                 </div>' ; 
        }
        ?>
        

                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
           
                    
    </body>
    <div class="footer">
        <p></p>
    </div>

    </html>


    <!-- later to be make separate php file -->

    <?php
  
    //if the form is submitted
    if (isset($_POST['submit']))
    {
         $name = $_POST['word'];
       
       
         $sentence = $_POST['sentence'];
         $category = $_POST['category'];
        $meaning = $_POST['meaning'];
        date("Y-m-d H:i:s");
    //Do some validation and escape sequence things
        
        $query = "UPDATE phrasalverb SET name='$name',sentence='$sentence',category = '$category',description='$meaning' ,date='$date' WHERE id = '".$_GET['key']."'
                                  "
            or die("query failed");
        $results = mysqli_query($conn, $query);
       
        
       if ( false===$results ) 
       {
            printf("error: %s\n", mysqli_error($conn));
        }
        else 
        {
            //show that word is added on the database
           echo '<script type="text/javascript">window.location = "PhrasalVerbDetails.php?id='.$_GET['key'].'"</script>';
            
            die();
        }
//        
    }
    
?>
