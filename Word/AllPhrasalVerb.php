

<!DOCTYPE html>
<html lang="en">

<head>
<?php require_once("../Includes/head.php"); ?>
    <style>
    #dropdown
        {
    float:right;
    display:block;
    clear:left;
    }
    
    </style>

</head>

<body>

        <!-- Navigation  -->
   <?php
    include "../Includes/nav.php";
   $selected ='';
    $query = '';
    $results = '';
    function get_options($selected){
    $categories = array('All Phrasal Verb' => 'all','hard' => 'hard', 'comfortable' => 'comfortable', 'easy' => 'easy');
    $options ='';
    while(list($k, $v) = each($categories))
    {
        if($selected === $v){
            $options .= '<option value="'.$v.'" selected>'.$k.'</option>';
        }
        else{
            $options .= '<option value="'.$v.'">'.$k.'</option>';
        }
        
        
    }
    return $options;

    }
    if(isset($_POST["dropdown"])){

     $selected = $_POST["dropdown"];
        if($selected == "hard")
        {
            //do hard stuff
            $query = "SELECT * FROM phrasalverb where category='hard' ORDER BY date DESC" or die("query died 1");
            $results = mysqli_query($conn, $query) or die("results died 1");
        }
        else if($selected == "comfortable")
        {
            //do comfortable stuff
            $query = "SELECT * FROM phrasalverb WHERE category='comfortable' ORDER BY date DESC" or die("query died 2");
            $results = mysqli_query($conn, $query) or die("results died 2");
        }
        else if($selected == "easy")
        {
            //do easy stuff
            $query = "SELECT * FROM phrasalverb WHERE category='easy' ORDER BY date DESC 3" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died 3");
        }
        else
        {
           //all words 
            $query = "SELECT * FROM phrasalverb ORDER BY date DESC" or die("query died 4");
            $results = mysqli_query($conn, $query) or die("results died 4");
        }
    }
    else{
         $query = "SELECT * FROM phrasalverb ORDER BY date DESC" or die("query died 5");
        $results = mysqli_query($conn, $query) or die("results died 5");
    }
    //echo get_options();
    
    
    echo '<div class="body_wrapper container">
     <a href="AddPhrasalVerb.php"><button class="btn btn-primary" >Add Word</button></a>
    <form action="'.$_SERVER["PHP_SELF"].'" method="POST" >
    <select id="dropdown" name="dropdown" onchange="this.form.submit();">
       '.get_options($selected).'
    </select>
    </form>

</div><br />';
    

    while($row = mysqli_fetch_array($results))
    {
  
    echo '<div class="container">
    <div class="panel panel-default">
            <div class="panel-heading">
                <a href="PhrasalVerbDetails.php?id='.$row['id'].'"><h3 class="panel-title">
                <!-- Actual word need to go here -->
               '. $row['name'] .'
                <!-- type of word goes here -->
                ['.' '. $row['description'] .' ' .']
                </h3> 
                </a>
            </div>
            <div class="panel-body">
               <!-- word description goes here -->
                '. $row['description'] .'
                <br />
    
            </div>
        </div>
    </div>';
    }
    ?>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>

</html>