<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once("../Includes/head.php"); ?>
</head>

<body>
    <!-- Navigation  -->
   <?php 
    include "../Includes/nav.php";
    $query = 'SELECT * FROM word WHERE wordkey = "'.$_GET['id'].'"' or die("query died");
    $results = mysqli_query($conn, $query) or die("results died");
    while($result = mysqli_fetch_array($results))
    {
    echo '<div class="body_wrapper container">
       <!-- ten recent words from hard category goes in first page -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                <!-- Actual word need to go here -->
               '. $result['name'] .'
                <!-- type of word goes here -->
                ['.' '. $result['type'] .' ' .']
                </h3> 
            </div>
            <div class="panel-body">
               
                '. $result['description'] .'
                <br />
                <br /> 
               
                Synonym: '. $result['synonym']. '
                
                <br />
                <br /> 
               
                Example Sentences: <br/>'. $result['sentence']. '
                <br />
                <br /> 
                <!-- need to make some validation so that the word doesnot repeat here. For instance, if word is verb then verb should not be here -->
                <pre>Adverb: ' .$result['adverb'].'  Adjective: '.$result['adjective'].' Noun: ' . $result['noun'].' Verb: ' . $result['verb'].'
                </pre>
                <h5>Focus On : '.$result['focuson'].' </h5>
                <a href="Edit.php?key='.$_GET['id'].'"><button type="submit" name="submit" id="submit" class="btn btn-primary">Edit</button></a>
            </div>
        </div>
    </div>';
    }
    ?>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>

</html>