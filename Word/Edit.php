<!DOCTYPE html>
    <html lang="en">

    <head>
 <?php require_once("../Includes/head.php"); ?>
        <script src="//cdn.ckeditor.com/4.5.11/basic/ckeditor.js"></script>
    </head>

    <body>

        <?php 
        include "../Includes/nav.php";
        $displayQuery = 'SELECT * FROM word WHERE wordkey = "'.$_GET['key'].'"' or die("query died");
    
        $displayResults = mysqli_query($conn, $displayQuery);
        if ($displayResults===false)
        {
          printf("error: %s\n", mysqli_error($conn));
        }
        while ($dResult = mysqli_fetch_array($displayResults)){
             $displayname = $dResult['name'];
            $displayCategory = $dResult['category'];
            $displayWordType = $dResult['type'];
            $displayFocusOn = $dResult['focuson'];
            $displayDescription = $dResult['description'];
            $displayAdverb = $dResult['adverb'];
            $displayNoun = $dResult['noun'];
            $displayVerb = $dResult['verb'];
            $displayAdjective = $dResult['adjective'];
            $displaySynonym = $dResult['synonym'];
            $displaySentence = $dResult['sentence'];
            
            $pronunciationselected = null;
            $meaningselected = null;
            $adjectiveselected = null;
                $adverbselected = null;
                $verbselected = null;
                $nounselected = null;
                $easy = null;
                $comf = null;
                $hard = null;
                
            
            
        
        //to make the right category checked
        if($displayCategory == "hard")
                    {
                        $hard = "checked";
                      
                    }
                    elseif($displayCategory == "comfortable")
                    {
                        $comf = "checked";
                     
                    }
                    else
                    {
                        $easy = "checked";
                     
                   }
                    
        
        //to make the right dropdown checked
        
        if($displayWordType == "noun")
                    {
                        $nounselected = "selected";
                      
                    }
                    elseif($displayWordType == "verb")
                    {
                        $verbselected = "selected";
                    }
                    elseif($displayWordType == "adverb")
                    {
                        $adverbselected = "selected";
        
                   }
                    else
                    {
                        $adjectiveselected = "selected";
                    
                     }
        //to make the right focus on checked
        if($displayFocusOn == "meaning")
        {
            $meaningselected = "checked";
        }
        elseif($displayFocusOn  == "pronunciation")
        {
            $pronunciationselected = "checked";
        }
        else if($meaningselected == null || $pronunciationselected == null )
        {
                $pronunciationselected = null;
            $meaningselected = null;
            
        }
        else{
            $meaningselected = "checked";
            $pronunciationselected = "checked";
            }
        
        
           echo '<div class="body_wrapper container">
           <form method="post" action="" onsubmit="return checkValidation()">
                    <div class="form-group">
                        <label for="word"> New Word</label>
                        <input type="text" class="form-control" id="word" name="word" value="'.$displayname.'" />
                    </div>
                    <div class="form-group">
                        <label for="word"> Description </label>
                        <textarea type="text" class="form-control" id="meaning" name="meaning">'.$displayDescription.'</textarea>
                         <script>
                            CKEDITOR.replace( "meaning" );
                        </script>
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="type" name="type" value="">
                            <option '.$nounselected .' value="noun">Noun</option>
                            <option '.$verbselected .' value="verb">Verb</option>
                            <option '.$adjectiveselected .' value="adjective">Adjective</option>
                            <option '.$adverbselected .' value="adverb">Adverb</option>
                             

                        </select>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Different Forms</div>
                        <div class="form-group col-sm-3">
                            <label for="word"> Adverb </label>
                            <input type="text" class="form-control" id="adverb" name="adverb" value="'.$displayAdverb.'">
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="word"> Noun </label>
                            <input type="text" class="form-control" id="noun" name="noun" value="'.$displayNoun.'">
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="word"> Verb </label>
                            <input type="text" class="form-control" id="verb" name="verb" value="'.$displayVerb.'">
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="word"> Adjective </label>
                            <input type="text" class="form-control" id="adjective" name="adjective" value="'.$displayAdjective.'">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="synonym">Synonym</label>
                        <input type="text" class="form-control" id="synonym" name="synonym" value="'.$displaySynonym.'">

                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea">Example Sentence</label>
                        <textarea class="form-control" id="sentence" name="sentence" rows="3"
                        >'.$displaySentence.'</textarea>
                        <script>
                            CKEDITOR.replace( "sentence" );
                        </script>
                    </div>
                    <fieldset class="form-group">
                   <legend>Category</legend>
                        <label class="radio-inline">
                            <input type="radio" name="category" value="hard" '.$hard.'>Hard</label>
                        <label class="radio-inline">
                            <input type="radio" name="category" value="comfortable"'.$comf .'>Comfortable</label>
                        <label class="radio-inline">
                            <input type="radio" name="category" value="easy"'.$easy .'>Easy</label>
                    </fieldset>

                    <fieldset class="form-group">
                        <legend>Focus on</legend>
                        <label class="checkbox-inline">

                            <input type="checkbox" name="focusOn[]" value="meaning" '.$meaningselected.'>Meaning</label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="focusOn[]" value="pronunciation"'.$pronunciationselected .'>Pronunciation</label>
                    </fieldset>
                    <button type="submit" name="submit" id="submit" class="btn btn-primary">Update</button>
                </form>
                 </div>' ; 
        }
        ?>
        

                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
           
                    
    </body>
    <div class="footer">
        <p></p>
    </div>

    </html>


    <!-- later to be make separate php file -->

    <?php
  
    //if the form is submitted
    if (isset($_POST['submit']))
    {
         $name = $_POST['word'];
         $type = $_POST['type'];
         $synonym = $_POST['synonym'];
         $sentence = $_POST['sentence'];
         $category = $_POST['category'];
        $meaning = $_POST['meaning'];          
        $adverb = $_POST['adverb'];
        $adjective = $_POST['adjective'];
        $verb = $_POST['verb'];
        $noun = $_POST['noun'];
        $date = date("Y-m-d H:i:s");
        $focusOn = null;
        //if checkbox is selected
       if(isset($_POST['focusOn'])){
           $focus = $_POST['focusOn'];
          $arrayCount = count($focus);
           echo $arrayCount;
        if($arrayCount == 1){
            $focusOn = $focus[0];
        }
        else
        {
            $focusOn = $focus[0].' , '. $focus[1];
        }
       
       }
        //Do some validation and escape sequence things
        
        $query = "UPDATE word SET name='$name',type ='$type',synonym='$synonym',sentence='$sentence',category = '$category',description='$meaning',adverb='$adverb',noun= '$noun',verb='$verb',adjective='$adjective',focuson= '$focusOn',dateAdded='$date' WHERE wordkey = '".$_GET['key']."'
                                  "
            or die("query failed");
        $results = mysqli_query($conn, $query);
       
        
       if ( false===$results ) 
       {
            printf("error: %s\n", mysqli_error($conn));
        }
        else 
        {
            //show that word is added on the database
           echo '<script type="text/javascript">window.location = "WordDetails.php?id='.$_GET['key'].'"</script>';
            
           // die();
        }
//        
    }
    
?>
