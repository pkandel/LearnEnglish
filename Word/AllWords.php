

<!DOCTYPE html>
<html lang="en">

<head>
<?php require_once("../Includes/head.php"); ?>
    <!-- css for dropdown -->
    <style>
    #dropdown
        {
    float:right;
    display:block;
    clear:left;
    }
    
    </style>

</head>

<body>

        <!-- Navigation  -->
   <?php
    include "../Includes/nav.php";

    echo '<div class="body_wrapper">';
   $selected ='';
    $query = '';
    $results = '';
    function get_options($selected){
    $categories = array('All Words' => 'allWords','hard' => 'hard', 'comfortable' => 'comfortable', 'easy' => 'easy');
    $options ='';
    while(list($k, $v) = each($categories))
    {
        if($selected === $v){
            $options .= '<option value="'.$v.'" selected>'.$k.'</option>';
        }
        else{
            $options .= '<option value="'.$v.'">'.$k.'</option>';
        }
        
        
    }
    return $options;

    }
    if(isset($_POST["dropdown"])){

     $selected = $_POST["dropdown"];
        if($selected == "hard")
        {
            //do hard stuff
            $query = "SELECT * FROM word where category='hard' ORDER BY name" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died");
        }
        else if($selected == "comfortable")
        {
            //do comfortable stuff
            $query = "SELECT * FROM word WHERE category='comfortable' ORDER BY name" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died");
        }
        else if($selected == "easy")
        {
            //do easy stuff
            $query = "SELECT * FROM word WHERE category='easy' ORDER BY name" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died");
        }
        else
        {
           //all words 
            $query = "SELECT * FROM word ORDER BY name" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died");
        }
    }
    else{
         $query = "SELECT * FROM word ORDER BY name" or die("query died");
        $results = mysqli_query($conn, $query) or die("results died");
    }
    //echo get_options();
    
    
    echo '<div class="container">
     <a href="http://personallearningsite.xyz/Word/AddWord.php"><button class="btn btn-primary" >Add Word</button></a>
    <form action="'.$_SERVER["PHP_SELF"].'" method="POST" >
    <select id="dropdown" name="dropdown" onchange="this.form.submit();">
       '.get_options($selected).'
    </select>
    </form>

</div><br />';
    

    while($row = mysqli_fetch_array($results))
    {
  
    echo '<div class="container">
    <div class="panel panel-default">
            <div class="panel-heading">
                <a href="http://personallearningsite.xyz/Word/WordDetails.php?id='.$row['wordkey'].'"><h3 class="panel-title">
                <!-- Actual word need to go here -->
               '. $row['name'] .'
                <!-- type of word goes here -->
                ['.' '. $row['type'] .' ' .']
                </h3> 
                </a>
            </div>
            <div class="panel-body">
               <!-- word description goes here -->
                '. $row['description'] .'
                <br />
                <br /> 
                <!--Synonym goes here -->
                Synonym: '. $row['synonym']. '
            </div>
        </div>
    </div>';
    }
    ?>
    </div>;
<?php require_once("../Includes/footer.php"); ?>
</body>

</html>