<?php  session_start(); ?>
<!DOCTYPE html>
<html>
<head>
  <?php 
  session_start();
  require_once("Includes/head.php"); ?>
  <style >
   a:link{
    text-decoration: none;
  }
  h3 {
    text-align: center;

  }
</style>
</head>
<body>
  <?php 	
  include('Includes/nav.php');
 // echo "<script> alert('admin = ".isset($_SESSION['admin'])."'); </script>";
      if($_SESSION['admin'] == 1)
   { 
  $results = MainSection::Get_All();
  ?>
  <div class="body_wrapper container">
    <a href="/Note/addNewMainSection.php"> <button class="btn btn-primary">Add New</button></a>
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th>
            Main Section Name
          </th>
          <th>
            Modify
          </th>
        </tr>
      </thead>
      <tbody>

       <?php foreach ($results as $result)
       {?>

        <tr>
         <td>
           <a href="/Note/secondsectionindex.php?id=<?php echo $result->id; ?>">
             <?php echo htmlspecialchars($result->name, ENT_QUOTES, 'UTF-8'); ?>
           </a>
         </td>
         <td>
           <a href="/Note/editMainSection.php?id=<?php echo $result->id; ?>">Edit </a>
           |<a href="/Note/deleteMainSection.php?id=<?php echo $result->id; ?>">Delete </a>
         </td>
       </tr>
       <?php } }?>
     </tbody>
   </table>
 </div>
 <?php require_once("Includes/footer.php"); ?>

</body>
</html>