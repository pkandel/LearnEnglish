<!DOCTYPE html>
<html lang="en">

<head>
<?php require_once("../Includes/head.php"); ?>
        <!-- css for dropdown -->
    <style>
    #dropdown
        {
    float:right;
    display:block;
    clear:left;
    }
    
    </style>
</head>

<body>
        <!-- Navigation  -->
   <?php 
    include "../Includes/nav.php";

    //for dropdown
    
    $selected ='';
    $query = '';
    $results = '';
    function get_options($selected){
    $categories = array('All Essay' => 'All Essay','Discussive' => 'Discussive', 'Argument' => 'Argument', 'Problem and Solution' => 'Problem and Solution');
    $options ='';
    while(list($k, $v) = each($categories))
    {
        if($selected === $v){
            $options .= '<option value="'.$v.'" selected>'.$k.'</option>';
        }
        else{
            $options .= '<option value="'.$v.'">'.$k.'</option>';
        }
        
        
    }
    return $options;

    }
    if(isset($_POST["dropdown"])){

     $selected = $_POST["dropdown"];
        if($selected == "Discussive")    
        {
            //do discussive stuff
            $query = "SELECT * FROM essay where category='discussive' and istemplate='no' ORDER BY date DESC" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died 1");
        }
        else if($selected == "Argument")
        {
            //do Argument stuff
            $query = "SELECT * FROM essay WHERE category='Argument' and istemplate='no'  ORDER BY date DESC" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died 2");
        }
        else if($selected == "Problem and Solution")
        {
            //do Problem and Solution stuff
            $query = "SELECT * FROM essay WHERE category='Problem and Solution' and istemplate='no'  ORDER BY date DESC" or die("query died" );
            $results = mysqli_query($conn, $query) or die("results died 3");
        }
        else
        {
           //all words 
            $query = "SELECT * FROM essay WHERE istemplate='no'  ORDER BY date DESC" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died 4");
        }
    }
    else{
         $query = "SELECT * FROM essay WHERE istemplate='no' ORDER BY date DESC" or die("query died ");
        $results = mysqli_query($conn, $query) or die("results died 5");
    }
    //echo get_options();
    
    
    echo '<div class="body_wrapper container">
    <a href="/Essay/WriteEssay.php"><button class="btn btn-primary">Write Essay</button></a>
    <form action="'.$_SERVER["PHP_SELF"].'" method="POST" >
    <select id="dropdown" name="dropdown" onchange="this.form.submit();">
       '.get_options($selected).'
    </select>
    </form>

</div><br />';
    
    while ( $result = mysqli_fetch_array($results))
    {
        echo '  
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-body">
                <a href="EssayDetails.php?id='.$result['id'].'">
                    Question '.$result['id'].' :-
                    '.$result['question'].'
                    </a>
                </div>
            </div>
        </div>';
    }
   ?>
 
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>

</html>