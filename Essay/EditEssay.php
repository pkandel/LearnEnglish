<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once("../Includes/head.php"); ?>
<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
</head>

</head>

<body>
    <!-- Navigation  -->
    <?php
    include "../Includes/nav.php";	
  if( isset($_SESSION['admin']) && $_SESSION['admin'] == 1) {

     $query = "SELECT * FROM essay WHERE id='".$_GET['key']."'";
    $results = mysqli_query($conn, $query);
    $Discussive = null;
    $Argument = null;
    $PandSolution = null;
    $Other = null;
    $Other2 = null;
    while ($result = mysqli_fetch_array($results))
    {
        if($result['category'] == "Discussive")
        {
             $Discussive = "selected";
        }
        elseif($result['category'] == "Argument")
        {
            $Argument = "selected";
        }
          elseif($result['category'] == "Problem and Solution")
        {
            $PandSolution = "selected";
        }
          else
        {
            $Other = "selected";
        }
        echo '        <div class="body_wrapper container">
            <form action="#" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <label for="exampleTextarea">Question</label>
                            <textarea class="form-control" id="question" name="question" rows="3">'.$result['question'].'</textarea>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="exampleTextarea">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="15">'.$result['description'].'</textarea>
                            <script>
                                CKEDITOR.replace( "description" );
                            </script>
                        </div>
                          <div class="form-group">
                    <label for="essayType">Essay Type</label>
                    <select class="form-control" id="type" name="type">
                        <option '.$Discussive.'>Discussive</option>
                        <option '.$Argument.'>Argument</option>
                        <option '.$PandSolution.'>Problem and Solution</option>
                        <option '.$Other.'>Advantage and Disadvantage</option>
                     

                    </select>
                </div>
                        <button type="submit" name="submit" id="submit" class="btn btn-primary" style="float:right">Save</button>
                    </div>

                </div>

            </form>
        </div>';
    }}
  else {
   echo "<div style='margin-top:80px; color: red'> <h1> You are not authorised </h1></div>";
  }
   ?>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

</body>

</html>

<?php
 require_once("../Includes/config.php");
if (isset($_POST['submit']))
{
    $question = $_POST['question'];
    $description = $_POST['description'];
    $category = $_POST['type'];
    $date = date("Y-m-d H:i:s");
    
    $query = "UPDATE essay SET question='$question', description='$description', category='$category', date='$date' WHERE id='".$_GET['key']."'" or die("query failed");
    $results = mysqli_query($conn, $query);
    if ( false===$results ) 
       {
            printf("error: %s\n", mysqli_error($conn));
        }
        else 
        {
            //show that word is added on the database
            echo '<script type="text/javascript">window.location = "/Essay/EssayDetails.php?id='.$_GET['key'].'"</script>';
            
            die();
        }
}

?>

