<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<?php require("../Includes/head.php");
	?>
  <script src="//cdn.ckeditor.com/4.6.0/full/ckeditor.js"></script>

</head>
<body>
<?php 
	  require("../Includes/nav.php");
if(isset($_SESSION['admin']) && $_SESSION['admin'] === 1)
                { 
	  $subSection = SubSection::Find_ById($_GET['id']);

	  
 ?>
 <div class="body_wrapper container">
<form action="#" method="post">
  <div class="form-group">
    <label for="name">Section Name</label>
    <input type="text" class="form-control" id="name" name="name" value='<?php echo $subSection->name; ?>' >
    </div>
      <div class="form-group">
    <label for="name">Content</label>

    <textarea class="form-control" id="content" name="content" rows="10" 
    value=<?php 
     $edit ="<xmp>".$subSection->content; echo $edit;?></textarea> 
<!--          <textarea class="form-control" id="content" name="content" rows="10" 
    value=<?php 
     $edit = $subSection->content; echo htmlspecialchars($edit);?></textarea> --> 

    </div>
  <button type="submit" name="submit" id="submit" class="btn btn-success">Submit</button> |
    <button type="submit" name="delete" id="delete" class="btn btn-danger">Delete</button>
</form>
</div>
 <?php }  else {
   echo "<div style='margin-top:80px; color: red'> <h1> You are not authorised </h1></div>";
  }
  
  ?>

<?php require_once("../Includes/footer.php"); ?>

</script>
</body>
</html>
<?php 
if (isset($_POST['submit']))
{
	$subSection->name = $_POST['name'];
	$subSection->content = $_POST['content'];
	$subSection->secondarysectionId = $_GET['sid'];
	$subSection->id = $_GET['id'];

	if($subSection->save())
		{
		echo '<script type="text/javascript">window.location = "subSectionIndex.php?id='.$_GET['sid'].'"</script>';
		}
}
if(isset($_POST['delete']))
{
    if($subSection->delete())
    {
       echo '<script type="text/javascript">window.location = "subSectionIndex.php?id='.$_GET['sid'].'"</script>'; 
    }
}

 ?>
               