<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
  <?php require("../Includes/head.php"); ?>
  <style >
   a:link{
    text-decoration: none;
  }
  h3 {
    text-align: center;

  }
</style>
</head>
<body>
  <?php 
  include('../Includes/nav.php');
if(isset($_SESSION['admin']) && $_SESSION['admin'] === 1)
                {  
  $results = SecondSection::Get_All();
  $MainSection = MainSection::Find_ById($_GET['id']);

  ?>
  <div class="body_wrapper container">

    <?php      
      //$record = MainSection::Find_ById(1);
      //echo $record->name;  

    ?>
    <a href="/Note/addNewSecondSection.php?id=<?php echo $_GET['id']; ?>"> <button class="btn btn-primary">Add New</button></a>
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th>
           <?php echo $MainSection->name; ?>
          </th>
          <th>
            Modify
          </th>
        </tr>
      </thead>
      <tbody>

       <?php foreach ($results as $result)
       if($result->mainsectionId == $_GET['id']){
       {?>

        <tr>
         <td>
           <a href="/Note/subSectionIndex.php?id=<?php echo $result->id; ?>">
             <?php echo htmlspecialchars($result->name, ENT_QUOTES, 'UTF-8'); ?>
           </a>
         </td>
         <td>
           <a href="/Note/editSecondSection.php?id=<?php echo $result->id; ?>&mid=<?php echo $MainSection->id; ?>">Edit </a>
           |<a href="/Note/deleteSecondSection.php?id=<?php echo $result->id; ?>&mid=<?php echo $MainSection->id; ?>">Delete </a>
         </td>
       </tr>
       <?php } }?>
     </tbody>
   </table>
 </div>
   <?php } else {
   echo "<div style='margin-top:80px; color: red'> <h1> You are not authorised </h1></div>";
  }  ?>
 <?php require_once("../Includes/footer.php"); ?>

</body>
</html>
               