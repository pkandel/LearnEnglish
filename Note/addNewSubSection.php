
<!DOCTYPE html>
<html>
<head>
	<?php require("../Includes/head.php");?>
    <script src="//cdn.ckeditor.com/4.6.0/full/ckeditor.js"></script>
</head>
<body>
<?php 
	  require("../Includes/nav.php");	
       if($_SESSION['admin'] == 1)
   { 
 ?>
 <div class="body_wrapper container">
<form action="#" method="post" id="section">
  <div class="form-group">
    <label for="name">Section Name</label>
    <input type="text" class="form-control" id="name" name="name">

<div id="name_valid" class="alert alert-danger alert-dismissible fade in">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Error!</strong> Name Field cannot be left Empty!!!
</div>



   </div>
     <div class="form-group">
    <label for="name">Content</label>
     <!-- <textarea id="content" name="content" class="ckeditor" rows="10"></textarea> -->
     <textarea id="content" name="content" class="form-control" rows="10"></textarea>

  <button name="submit" id="submit" class="btn btn-success">Submit</button>
</form>
</div>
<?php require_once("../Includes/footer.php"); ?>
<script>
$(document).ready(function(){
  $("#name_valid").hide();
  $("#submit").click(function(e){
      if($("#name").val()=="")
      {
        e.preventDefault();
        $("#name_valid").fadeIn();

      }
  });

});
// validation needs to be done
</script>
</body>
</html>
<?php 
if (isset($_POST['submit']))
{
	$subSection = new subSection();
	$subSection->name = $_POST['name'];
	$subSection->content = $_POST['content'];
	$subSection->secondsectionId = $_GET['id'];
	if($subSection->save())
		{
		 echo '<script type="text/javascript">window.location = "subSectionIndex.php?id='.$_GET['id'].'"</script>';
		}
}

 ?>
                <?php } ?>