<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<?php require("../Includes/head.php");
	?>
</head>
<body>
<?php 
	  require("../Includes/nav.php");

if(isset($_SESSION['admin']) && $_SESSION['admin'] === 1)
                {  
	  $mainSection = MainSection::Find_ById($_GET['id']);

	  
 ?>
 <div class="body_wrapper container">
<form action="#" method="post">
  <div class="form-group">
    <label for="name">Section Name</label>
    <input type="text" class="form-control" id="name" name="name" value='<?php echo $mainSection->name ?>' >
    </div>
  <button type="submit" name="submit" id="submit" class="btn btn-success">Submit</button>
</form>
</div>
  <?php } else {
   echo "<div style='margin-top:80px; color: red'> <h1> You are not authorised </h1></div>";
  }?>
<?php require_once("../Includes/footer.php"); ?>
</body>
</html>
<?php 
if (isset($_POST['submit']))
{
	$mainSection->name = $_POST['name'];
	if($mainSection->save())
		{
		echo '<script type="text/javascript">window.location = "../Note.php"</script>';
		}
}

 ?>
								