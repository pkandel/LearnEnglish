 <?php session_start(); ?>
<!doctype html>
<html lang="en">
<head>

    <?php require ("../Includes/head.php");?>
     <?php require ("../Includes/highlighter.php");?>
    <script type="text/javascript">
		$(document).ready(function(){
		SyntaxHighlighter.all();
		// var replaced = $("body").html().hide('<xmp>');
		// $("body").html(replaced);
		});
    </script>
    <link rel="stylesheet" href="../CSS/subSectionIndex.css" />
  
</head>
<body>
    <?php 
    require ("../Includes/subSectionIndexnav.php");
   if(isset($_SESSION['admin']) && $_SESSION['admin'] === 1)
                { 
    //get_all_order_by gives the sorted order on passing whatever needs to sort
    $subSection = SubSection::Get_All_Order_By("name");
    $secondSection = secondSection::Find_ById($_GET['id']);

    ?>

    <div class="body_wrapper container">

        <div id="tabs">
            <div id="leftPane" class="leftPane">
            <h4>
              
                    <a href="secondsectionindex.php?id=<?php echo $secondSection->mainsectionId; ?>">
                        <?php echo htmlspecialchars($secondSection->name,ENT_QUOTES, 'UTF-8'); ?>
                    </a>
</h4>
<hr/>
                

         
                <ul id="myul">
                    <?php foreach ($subSection as $section){
                        if($section->secondsectionId == $_GET['id'])

                        {

                            ?>
                            <li><a href="#tabs-<?php echo $section->id; ?>"><?php echo htmlspecialchars($section->name, ENT_QUOTES, 'UTF-8');?></a></li>
                            <?php }} ?>
                </ul>
               
                    </div>
                    <div id="rightPane" class="rightPane">
                            <a href="addNewSubSection.php?id=<?php echo $_GET['id']; ?>">
                                <button type="button" class="btn btn-primary btn-sm" name="create"
                                style="float:right;">
                                       Add 
                            </button>
                            </a>
                         

                        
                        <?php
                        foreach ($subSection as $section){
                            if($section->secondsectionId == $_GET['id'])  { ?>

                                <div id="tabs-<?php echo $section->id; ?>">
                                    <h4 style="text-align:center"> 
                                        <a href="editSubSection.php?id=<?php echo $section->id; ?>&sid=<?php echo $secondSection->id; ?>">
                                            <?php echo htmlspecialchars($section->name, ENT_QUOTES, 'UTF-8');?>
                                        </a>    
                                    </h4>
                                    <hr/>                                      
                                         <?php 
                                            $content = $section->content; 
                                            echo $content;
                                            ?> 

                                    
                                </div>
                                <?php }} ?>
                            </div>

                        </div>
                <?php } else {
   echo "<div style='margin-top:80px; color: red'> <h1> You are not authorised </h1></div>";
  } ?>
                         <?php require_once ("../Includes/footer.php"); ?>
                        <script src = "../JavaScript/subSectionIndex.js"></script>   
                     
                    </body>
                    </html>


      
