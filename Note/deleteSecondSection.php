    <?php  ?>

<!doctype html>
<html lang="en">
<head>
<?php require ("../Includes/head.php"); ?>
</head>
<body>
<?php

require ("../Includes/nav.php");
if(isset($_SESSION['admin']) && $_SESSION['admin'] === 1)
                {
$record = SecondSection::Find_ById($_GET['id']);

?>
<div class="body_wrapper container">
    <form method="post" action="#">
    <div class="panel panel-danger">
        <div class="panel-heading">
                Are You Sure Want to Delete It?
            </div>
            <div class="panel-body">
               Name: <?php echo htmlentities($record->name,ENT_QUOTES, 'UTF-8'); ?>
            </div>
            <div class="padel-footer">
                <a href="../Note.php" class="btn btn-success" style="margin:10px;" >Back</a> |
                <button class="btn btn-danger" style="margin:10px;" name="submit" value="Delete">Delete</button>

            </div>
    </div>
    </form>

</div>
  <?php }  else {
   echo "<div style='margin-top:80px; color: red'> <h1> You are not authorised </h1></div>";
  } ?>
</body>
</html>
<?php if(isset($_POST['submit']))
{
    $record->delete();
    echo '<script type="text/javascript">window.location = "secondsectionindex.php?id='.$_GET['mid'].'"</script>';
}
?>
              