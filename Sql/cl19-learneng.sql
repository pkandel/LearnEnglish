-- phpMyAdmin SQL Dump
-- version 4.0.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 18, 2016 at 12:55 PM
-- Server version: 5.5.47
-- PHP Version: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Table structure for table essay
--

CREATE TABLE IF NOT EXISTS essay (
  id int(11) NOT NULL AUTO_INCREMENT,
  question varchar(1000) NOT NULL,
  description varchar(10000) NOT NULL,
  category varchar(100) NOT NULL,
  date date NOT NULL,
  istemplate varchar(5) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table essay
--

--
-- Table structure for table phrasalverb
--

CREATE TABLE IF NOT EXISTS phrasalverb (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  description varchar(10000) NOT NULL,
  category varchar(100) NOT NULL,
  date date NOT NULL,
  sentence varchar(10000) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table speakinggraph
--

CREATE TABLE IF NOT EXISTS speakinggraph (
  id int(11) NOT NULL AUTO_INCREMENT,
  question varchar(1000) NOT NULL,
  description mediumtext NOT NULL,
  category varchar(50) NOT NULL,
  date date NOT NULL,
  istemplate varchar(10) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table summarise
--

CREATE TABLE IF NOT EXISTS summarise (
  id int(11) NOT NULL AUTO_INCREMENT,
  question varchar(10000) NOT NULL,
  answer varchar(1000) NOT NULL,
  category varchar(100) NOT NULL,
  date date NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table word
--

CREATE TABLE IF NOT EXISTS word (
  wordkey int(11) NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL,
  synonym varchar(50) DEFAULT NULL,
  sentence varchar(500) DEFAULT NULL,
  category varchar(15) NOT NULL,
  type varchar(15) NOT NULL,
  description varchar(100) NOT NULL,
  adverb varchar(50) DEFAULT NULL,
  noun varchar(50) DEFAULT NULL,
  verb varchar(50) DEFAULT NULL,
  adjective varchar(50) DEFAULT NULL,
  focuson varchar(50) NOT NULL,
  dateAdded date NOT NULL,
  PRIMARY KEY (wordkey)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
