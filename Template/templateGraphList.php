<!DOCTYPE html>
<html lang="en">

<head>
<?php require("../Includes/head.php"); ?>
<style type="text/css" src="../CSS/style.css"></style>
    <style>
    #dropdown
        {
    float:right;
    display:block;
    clear:left;
    }
    
    </style>
</head>

<body>
        <!-- Navigation  -->
   <?php 
    include "../Includes/nav.php";

    //for dropdown
    
    $selected ='';
    $query = '';
    $results = '';
    function get_options($selected){
    $categories = array('All Graph' => 'All Graph','Pie Chart' => 'Pie', ' Graph' => 'Graph', 'Picture' => 'Picture', 'Life Cycle' => 'Life Cycle', 'Map' => 'Map','Org Chart' => 'Org Chart','Table Chart' => 'Table Chart', 'Time Table' => 'Time Table');
    $options ='';
    while(list($k, $v) = each($categories))
    {
        if($selected === $v){
            $options .= '<option value="'.$v.'" selected>'.$k.'</option>';
        }
        else{
            $options .= '<option value="'.$v.'">'.$k.'</option>';
        }
        
        
    }
    return $options;

    }
    if(isset($_POST["dropdown"])){

     $selected = $_POST["dropdown"];
        if($selected == "Pie")    
        {
            //do discussive stuff
            $query = "SELECT * FROM speakinggraph where category='Pie' and istemplate='yes' ORDER BY id" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died 1");
        }
        else if($selected == "Graph")
        {
            //do Argument stuff
            $query = "SELECT * FROM speakinggraph WHERE category='Graph' and istemplate='yes'  ORDER BY id" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died 2");
        }
        else if($selected == "Picture")
        {
            //do Problem and Solution stuff
            $query = "SELECT * FROM speakinggraph WHERE category='Picture' and istemplate='yes'  ORDER BY id" or die("query died" );
            $results = mysqli_query($conn, $query) or die("results died 3");
        }
                else if($selected == "Life Cycle")
        {
            //do Problem and Solution stuff
            $query = "SELECT * FROM speakinggraph WHERE category='Life Cycle' and istemplate='yes'  ORDER BY id" or die("query died" );
            $results = mysqli_query($conn, $query) or die("results died 3");
        }
                else if($selected == "Map")
        {
            //do Problem and Solution stuff
            $query = "SELECT * FROM speakinggraph WHERE category='map' and istemplate='yes'  ORDER BY id" or die("query died" );
            $results = mysqli_query($conn, $query) or die("results died 3");
        }
                  else if($selected == "Org Chart")
        {
            //do Problem and Solution stuff
            $query = "SELECT * FROM speakinggraph WHERE category='Org Chart' and istemplate='yes'  ORDER BY id" or die("query died" );
            $results = mysqli_query($conn, $query) or die("results died 3");
        }
                  else if($selected == "Table Chart")
        {
            //do Problem and Solution stuff
            $query = "SELECT * FROM speakinggraph WHERE category='Table Chart' and istemplate='yes'  ORDER BY id" or die("query died" );
            $results = mysqli_query($conn, $query) or die("results died 3");
        }
                  else if($selected == "Time Table")
        {
            //do Problem and Solution stuff
            $query = "SELECT * FROM speakinggraph WHERE category='Time Table' and istemplate='yes'  ORDER BY id" or die("query died" );
            $results = mysqli_query($conn, $query) or die("results died 3");
        }
        else
        {
           //all words 
            $query = "SELECT * FROM speakinggraph WHERE istemplate='yes'  ORDER BY id" or die("query died");
            $results = mysqli_query($conn, $query) or die("results died 4");
        }
    }
    else{
         $query = "SELECT * FROM speakinggraph WHERE istemplate='yes'  ORDER BY id" or die("query died ");
        $results = mysqli_query($conn, $query) or die("results died 5");
    }
    //echo get_options();
    
    
    echo '<div class="body_wrapper container">
   <a href="/Template/templateSpeakingGraph.php"><button class="btn btn-primary" >Add Template</button></a>
    <form action="'.$_SERVER["PHP_SELF"].'" method="POST" >
    <select id="dropdown" name="dropdown" onchange="this.form.submit();">
       '.get_options($selected).'
    </select>
    </form>

</div><br />';
    
    while ( $result = mysqli_fetch_array($results))
    {
        echo '  
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-body">
                <a href="/Speaking/speakingDetails.php?id='.$result['id'].'">
                    Question '.$result['id'].' :-
                    '.$result['question'].'
                    </a>
                </div>
            </div>
        </div>';
    }
   ?>
 
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>

</html>