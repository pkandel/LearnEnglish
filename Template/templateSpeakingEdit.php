<!DOCTYPE html>
<html lang="en">

<head>
<?php require("../Includes/head.php"); ?>
<style type="text/css" src="../CSS/style.css"></style>
<!--rich text editor -->
<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
</head>

</head>

<body>
    <!-- Navigation  -->
    <?php 
    include "../Includes/nav.php";
    require("../Includes/config.php");
     $query = "SELECT * FROM speakinggraph WHERE id='".$_GET['key']."'";
    $results = mysqli_query($conn, $query);
    
    
    while ($result = mysqli_fetch_array($results))
    {
        if($result['category'] == "pie")
        {
             $pie = "selected";
        }
        elseif($result['category'] == "line")
        {
            $line = "selected";
        }
          elseif($result['category'] == "picture")
        {
            $picture = "selected";
        }
          elseif($result['category'] == "lc")
        {
            $lc = "selected";
        }
        elseif($result['category'] == "map")
        {
            $map = "selected";
        }
          else
        {
            $bar = "selected";
        }
        echo '        <div class="container">
            <form action="#" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <label for="exampleTextarea">Question</label>
                            <textarea class="form-control" id="question" name="question" rows="3">'.$result['question'].'</textarea>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="exampleTextarea">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="15">'.$result['description'].'</textarea>
                            <script>
                                CKEDITOR.replace( "description" );
                            </script>
                        </div>
                          <div class="form-group">
                    <label for="essayType">Image Type</label>
                    <select class="form-control" id="type" name="type">
                        <option value = "pie" '.$pie.'>Pie Chart</option>
                        <option value = "line" '.$line.'>Line Graph</option>
                        <option value = "picture" '.$picture.'>Picture</option>
                        <option value = "lc" '.$lc.'>Life Cycle</option>
                        <option value = "map" '.$map.'>Map</option>
                        <option value = "bar" '.$bar.'>Bar Graph</option>

                    </select>
                </div>
                        <button type="submit" name="submit" id="submit" class="btn btn-primary" style="float:right">Save</button>
                    </div>

                </div>

            </form>
        </div>';
    }
   ?>

<?php require_once("../Includes/footer.php"); ?>
</body>

</html>

<?php
 require_once("Includes/config.php");
if (isset($_POST['submit']))
{
    $question = $_POST['question'];
    $description = $_POST['description'];
    $category = $_POST['type'];
    $date = date("Y-m-d H:i:s");
    
    $query = "UPDATE speakinggraph SET question='$question', description='$description', category='$category', date='$date' WHERE id='".$_GET['key']."'" or die("query failed");
    $results = mysqli_query($conn, $query);
    if ( false===$results ) 
       {
            printf("error: %s\n", mysqli_error($conn));
        }
        else 
        {
            //show that word is added on the database
            echo '<script type="text/javascript">window.location = "speakingDetails.php?id='.$_GET['key'].'"</script>';
            
            die();
        }
}

?>
