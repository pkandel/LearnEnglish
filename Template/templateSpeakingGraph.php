<!DOCTYPE html>
<html lang="en">

<head>
<?php require("../Includes/head.php"); ?>
<style type="text/css" src="../CSS/style.css"></style>

<!--rich text editor -->
<script src="//cdn.ckeditor.com/4.6.0/full/ckeditor.js"></script>
</head>

<body>
    <!-- Navigation  -->
    <?php 
    include "../Includes/nav.php";
   ?>
        <div class="body_wrapper container">
            <form action="" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <label for="exampleTextarea">Image Title</label>
                            <textarea class="form-control" id="question" name="question" rows="2"></textarea>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="exampleTextarea">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="15"></textarea>
                            <script>
                                CKEDITOR.replace( "description" );
                            </script>
                        </div>
                          <div class="form-group">
                    <label for="essayType">Image Type</label>
                    <select class="form-control" id="type" name="type">
                        <option value="Pie">Pie Chart</option>
                        <option value="Graph">Graph</option>
                        <option value="Picture">Picture</option>
                         <option value="Life Cycle">Life Cycle</option>
                        <option value="Map">Map</option>
                        <option value="Org Chart">Org Chart</option>
                        <option value="Table Chart">Table Chart</option>
                        <option value="Time Table">Time Table</option>
        
                    </select>
                </div>
                        <button type="submit" name="submit" id="submit" class="btn btn-primary" style="float:right">Save</button>
                    </div>

                </div>

            </form>
        </div>
<?php require_once("../Includes/footer.php"); ?>
</body>

</html>

<?php
 require_once("../Includes/config.php");
if (isset($_POST['submit']))
{
    $question = $_POST['question'];
    $description = $_POST['description'];
    $category = $_POST['type'];
    $date = date("Y-m-d H:i:s");
    $istemplate = "yes";
    
    $query = "INSERT INTO speakinggraph (question,description,category,date,istemplate) values ('$question','$description','$category','$date','$istemplate')" or die("query failed");
    $results = mysqli_query($conn, $query);
    if ( false===$results ) 
       {
            printf("error: %s\n", mysqli_error($conn));
        }
        else 
        {
            //show that word is added on the database
               echo '<script type="text/javascript">window.location = "http://personallearningsite.xyz/Template/templateGraphList.php"</script>';
            
            die();
            
        }
}

?>
