<!DOCTYPE html>
<html lang="en">

<head>
<?php require("../Includes/head.php"); ?>

<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
</head>

<body>
    <!-- Navigation  -->
    <?php 
    include "../Includes/nav.php";
   ?>
        <div class="body_wrapper container">
            <form action="" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <label for="exampleTextarea">Question</label>
                            <textarea class="form-control" id="question" name="question" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="exampleTextarea">Description</label>
                           
                            <textarea class="form-control" id="description" name="description" rows="15"></textarea>
                                
                                <script>
                                        CKEDITOR.replace( 'description' );
                                </script>
                                
                                  
                           
                        </div>
                          <div class="form-group">
                    <label for="essayType">Essay Type</label>
                    <select class="form-control" id="type" name="type">
                        <option value="Discussive">Discussive</option>
                        <option value="Argument">Argument</option>
                        <option value="Problem and Solution">Problem and Solution</option>
                        <option value="Advantage and Disadvantage">Advantage and Disadvantage</option>

                    </select>
                </div>
                        <button type="submit" name="submit" id="submit" class="btn btn-primary" style="float:right">Save</button>
                    </div>

                </div>

            </form>
        </div>
<?php require_once("../Includes/footer.php"); ?>

          


</body>

</html>

<?php
 require_once("../Includes/config.php");
if (isset($_POST['submit']))
{
    $question = $_POST['question'];
    $description = $_POST['description'];
    $category = $_POST['type'];
    $date = date("Y-m-d H:i:s");
    $istemplate = "yes";
    
    $query = "INSERT INTO essay (question, description, category, date,istemplate) values ('$question','$description','$category','$date','$istemplate')" or die("query failed");
    $results = mysqli_query($conn, $query);
    if ( false===$results ) 
       {
            printf("error: %s\n", mysqli_error($conn));
        }
        else 
        {
            //show that word is added on the database
               echo '<script type="text/javascript">window.location = "/Template/templateEssay.php"</script>';
            
            die();
            
        }
}

?>
